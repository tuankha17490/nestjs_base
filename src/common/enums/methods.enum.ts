export enum MethodName{
  GET = 'GET',
  PUT = 'PUT',
  PATCH = 'PATCH',
  DELETE = 'DELETE',
  GET_LIST = 'GETLIST',
  POST = 'POST',
  DELETE_SOFT = 'DELETESOFT',
  MANAGE_ROLE = 'MANAGEROLE'
}
