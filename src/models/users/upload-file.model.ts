export interface UploadFile extends File {
  path: string
}
